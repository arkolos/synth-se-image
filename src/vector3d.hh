#ifndef VECTOR3D_HH
# define VECTOR3D_HH

# include "point.hh"
# include <math.h>
# include <string>

class Vector3d
{
public:
  Vector3d()
    : point_()
  {}
  Vector3d(const float x, const float y,const float z)
    : point_(x, y, z)
  {}
  Vector3d(const Point& p1, const Point& p2)
    : point_(p2.x() - p1.x(), p2.y() - p1.y(), p2.z() - p1.z())
  {}
  Vector3d(std::string str)
    : point_(str) // "x;y;z"
  {}
  float length() const;
  float length_squared() const
  {
    return point_.x() * point_.x() + point_.y() * point_.y()
              + point_.z() * point_.z();
  }
  Vector3d operator*(int a)const;
  Vector3d operator*(float a) const;
  Vector3d normed() const;
  Point get_point() const
  {
    return point_;
  }
  float x() const
  {
      return point_.x();
  }
  float y() const
  {
      return point_.y();
  }
  float z() const
  {
      return point_.z();
  }
  Point operator+(Point p) const;
  Vector3d normalize();
  float operator*(Vector3d other) const;
  Vector3d operator+(Vector3d p) const;
  Vector3d operator-() const
  {
    return *this * (-1);
  }
  Vector3d operator-(const Vector3d& p) const
  {
    return *this + (-p);
  }


  Vector3d operator^(Vector3d other) const;
  Vector3d get_reflection_with_normal(const Vector3d& normal) const; //normal should be normalized

private:
  Point point_; // vector = from origin until this point
};

std::ostream& operator<<(std::ostream& ostr, Vector3d const& c);

#endif /* !VECTOR3D_HH */
