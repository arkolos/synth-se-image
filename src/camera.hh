#ifndef CAMERA_HH
# define CAMERA_HH

# include "point.hh"
# include "vector3d.hh"

class Camera
{
public:
  Camera();
  Camera(Point p, Vector3d u, Vector3d v);

  Point get_origin() const;
  Vector3d get_u() const
  {
    return u_;
  }
  Vector3d get_v() const
  {
    return v_;
  }
  Vector3d calcul_w() const;

private:
  Point origin_;
   Vector3d u_;
   Vector3d v_;
};

#endif /* !CAMERA_H */
