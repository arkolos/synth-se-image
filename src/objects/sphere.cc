#include "sphere.hh"
#include <cassert>

#include <utility>

static float sqr(float x)
{
    return x * x;
}

Point Sphere::intersec_ray(const Ray &ray) const
{
    const Point pa = ray.get_start();
    const Point pb = ray.get_direction().normed() + ray.get_start();

    const Vector3d ray_dir = ray.get_direction().normed();

    const float a = sqr(pb.x() - pa.x()) + sqr(pb.y() -pa.y()) + (sqr(pb.z()
                - pa.z()));
    const float b = 2 * ((pb.x() - pa.x()) * (pa.x() - center_.x()) + (pb.y()
                - pa.y()) * (pa.y() - center_.y()) + (pb.z() - pa.z())
                  * (pa.z() - center_.z()));
    const float c = sqr(pa.x() - center_.x()) + sqr(pa.y() - center_.y())
              + sqr(pa.z() - center_.z()) - sqr(radius_);
    const float delta = sqr(b) - 4 * a * c;

    if (delta < 0) // no intersection
        return Point(0, 0, 0);
    if (delta == 0) // single intersection point
    {
        const float d = -b / (2 * a);
        return  ray_dir * d + pa;
    }
    // two intersection points
    float d1 = (-b - sqrt(delta)) / (2 * a);
    float d2 = (-b + sqrt(delta)) / (2 * a);

    if (d1 > d2)
    {
         std::swap(d1,d2);
    }

    if (d1 < 0. && d2 < 0.)
        return Point(0, 0, 0);
    else if (d1 < 0.)
        return ray_dir * d2 + pa;
    else
        return ray_dir * d1 + pa ;
}

Vector3d Sphere::normal_dir(const Point &point) const
{
  Vector3d vect(center_, point);

  return vect;
}

