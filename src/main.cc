#include "screenproj.hh"

#include "scene.hh"


#include "vector3d.hh"
int main(int argc, char *argv[])
{
    if (argc < 3)
        std::cerr << "usage: ./rt input.xml output.ppm" << std::endl;
    else
    {
        char *input_xml = argv[1];
        char *output_ppm = argv[2];
        Scene scene(input_xml);
        scene.render(output_ppm);
    }
}
