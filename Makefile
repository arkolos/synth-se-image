CXX=g++
CXXFLAGS=-Wall -Wextra -std=c++11 -pedantic -g3 -I /usr/local/include -I /opt/X11/include
CPPFLAGS=-MMD
S_SRC=main.cc point.cc vector3d.cc ray.cc physical-object.cc objects/sphere.cc\
light.cc directional-light.cc color.cc screenproj.cc exceptions/color-out-of-bound.cc\
scene.cc camera.cc exceptions/parseerror-exception.cc objects/plane.cc \
ambient-light.cc point-light.cc
DEPS=$(SRC:.cc=.d)
SRC=$(addprefix src/, $(S_SRC))
OBJS=$(SRC:.cc=.o)
OUT=rt
LDLIBS=-ltbb -L/opt/X11/lib -lX11 -pthread

all: $(OUT)

$(OUT): $(OBJS)
	$(CXX) $(CPPFLAGS) $(LDFLAGS) $(CXXFLAGS) -o $(OUT) $(OBJS) $(LDLIBS)

clean:
	$(RM) $(OBJS) $(OUT) $(OUT).core $(DEPS)

distclean: clean
	$(RM) makefile.rules

check: $(OUT)
	tests/check.sh
	./$(OUT) tests/xml/basic.xml output.ppm
	display output.ppm
	$(RM) output.ppm

demo: $(OUT)
	./$(OUT) scene/lum_point.xml output.ppm
	display output.ppm
	./$(OUT) scene/color.xml output.ppm
	display output.ppm
	./$(OUT) scene/superposition.xml output.ppm
	display output.ppm
	./$(OUT) scene/planes.xml output.ppm
	display output.ppm
	./$(OUT) scene/snowman.xml output.ppm
	display output.ppm
	./$(OUT) scene/plancut.xml output.ppm
	display output.ppm
	./$(OUT) scene/alight.xml output.ppm
	display output.ppm
	./$(OUT) scene/lum_point_2.xml output.ppm
	display output.ppm


-include makefile.rules $(DEPS)

.PHONY: all clean distclean check check-planes
