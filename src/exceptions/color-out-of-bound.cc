#include "color-out-of-bound.hh"

const char* ColorOutOfBound::what() const throw()
{
    return "Red, Green and Blue must be between 0 and 1.";
}
