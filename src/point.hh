
#ifndef POINT_HH
# define POINT_HH

# include <string>

class Point
{
public:
  Point()
    : x_(0)
    , y_(0)
    , z_(0)
  {};
  Point(float x, float y, float z)
    : x_(x)
    , y_(y)
    , z_(z)
  {};
  Point(std::string str); // str : X;Y;Z
  float x() const
  {
    return x_;
  }
  float y() const
  {
    return y_;
  }
  float z() const
  {
    return z_;
  }
  Point operator+(const Point p);
  Point operator-(const Point p);

  static float sqr(float x)
{
    return x * x;
}

  static float distance_square(Point a, Point b){
    return (sqr(a.x() - b.x()) + sqr(a.y() - b.y()) + sqr(a.z() - b.z()));
  }

private:
  float x_;
  float y_;
  float z_;
};

std::ostream& operator<<(std::ostream& ostr, Point const& c);
#endif /* !POINT_HH */
