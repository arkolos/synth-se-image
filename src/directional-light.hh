#ifndef DIRECTIONAL_LIGHT_HH
# define DIRECTIONAL_LIGHT_HH

# include "vector3d.hh"
# include "point.hh"
# include "light.hh"

class DirectionalLight : public Light
{
public:
  DirectionalLight(Color color, float diff, Vector3d direction)
    : Light(color, diff)
    , direction_(direction)
  {}
  ~DirectionalLight()
  {}
  virtual float get_light_inpoint(Point point, Vector3d normal) const override;
  virtual float get_specular_inpoint(const Point& point, const Vector3d& normal, const Vector3d reflection_user) const override;
  virtual bool is_shadow_in_point(const Point& point, const PhysicalObject* object, const std::vector<PhysicalObject*>& objects) const override;
  virtual Ray get_ray_topoint(const Point& point) const override;

private:
  const Vector3d direction_;
};
#endif /* !DIRECTIONAL_LIGHT_HH */
