#include "point-light.hh"

#include <cassert>

float PointLight::get_light_inpoint(Point point,
                                          Vector3d normal) const
{
  const Vector3d vect(origin_, point);
  const float product = normal.normed() * vect.normed();
  const float prod2 = product < 0 ? -product: 0;
  assert(prod2 >= 0 && prod2 <= 1);
  return prod2;
}


float PointLight::get_specular_inpoint(const Point& point, const Vector3d&, const Vector3d reflection_user) const
{
	const Vector3d vect(origin_, point);

	const float product = - vect.normed() * reflection_user.normed() ;

	return product > 0 ? product*product : 0;


	return 0.0f;

}

//pas meme fonction que pour directional
static bool is_ray_intersecting_objects(Ray& ray, const std::vector<PhysicalObject*> objects, const PhysicalObject* object_not_considered)
{
    for (PhysicalObject* el : objects)
    {
      if (el != object_not_considered)
      {   
        const Point p = el->intersec_ray(ray);

        if (!(p.x() == 0 && p.y() == 0 && p.z() == 0) && ray.coef_position_point(p) < 1.) //si il y a une intersection et qu'elle se trouve avant la lumiere (si ya un obstacle apres on s'en fiche)
            return true;
      }

    }
    return false;
}

bool PointLight::is_shadow_in_point(const Point& point,const PhysicalObject* object, const std::vector<PhysicalObject*>& objects) const
{
    Ray ray(point, origin_);
    return is_ray_intersecting_objects(ray, objects, object);
}

Ray PointLight::get_ray_topoint(const Point& point) const
{
    Ray ray(origin_, point);
    return ray;
}

