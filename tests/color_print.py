#! /usr/local/bin/env python2.7
# -*- coding: utf-8 -*-

def green(s):
    print('\033[92m' + s + '\033[0m')

def blue(s):
    print('\033[96m' + s + '\033[0m')

def fail(s, prefixe=""):
    print(prefixe + '\033[91m' + s + '\033[0m')

def warning(s):
    print('\033[93m' + s + '\033[0m')

def header(s):
    print('\033[95m' + s + '\033[0m')

def cmd_fail(cmd, s=""):
    print(s + '\033[91m' + "fail" + '\033[0m'),
    print(repr(cmd))

def cmd_fail_mem(cmd, s=""):
    print(s + '\033[91m' + "leak" + '\033[0m'),
    print(repr(cmd))

def cmd_success(cmd, s=""):
    print(s + '\033[92m' + "ok  " + '\033[0m'),
    print(repr(cmd))

def result(n):
    if n == 100:
        green(str(n) + " %")
    else:
        fail(str(n) + " %")

def result_n(n,total):
    if n == total:
        green(str(n) + "/" + str(total))
    else:
        fail(str(n) + "/" + str(total))
