#include "screenproj.hh"
#include "CImg.h"

Screenproj::Screenproj(int width, int height)
    : width_(width)
    , height_(height)
{
    image_.resize(width);
    for (int i = 0; i < width; i++)
        image_[i].resize(height);
    for (int x = 0; x < width; x++)
        for (int y = 0; y < height; y++)
            image_[x][y] = Color(0, 0, 0);
}

void Screenproj::set_color(int x, int y, const Color& c)
{
    image_[x][y] = c;
}

void Screenproj::save(const char *output_ppm)
{
    cimg_library::CImg<uint8_t> image_out(width_,height_,1,3);
    
     for (int y = 0; y < height_; y++)
        for (int x = 0; x < width_; x++)
        {
            const float c[] = {image_[x][y].r()*255, image_[x][y].g()*255 , image_[x][y].b()*255};
            image_out.draw_point(x, y, c);
        }
    image_out.display("Image");

    std::ofstream file_out;
    file_out.open(output_ppm);
    if (file_out.is_open() != 0)
    {
        file_out << "P3" << std::endl;
        file_out << width_ << " " << height_ << std::endl;
        file_out << "255" << std::endl;
        for (int y = 0; y < height_; y++)
        {
            for (int x = 0; x < width_ - 1; x++)
                file_out << image_[x][y] << "  ";
            file_out << image_[width_ - 1][y] << std::endl;
        }
    }
    file_out.close();
}
