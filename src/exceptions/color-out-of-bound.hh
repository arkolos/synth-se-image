#ifndef COLOR_OUT_OF_BOUND_HH
# define COLOR_OUT_OF_BOUND_HH

# include <exception>

class ColorOutOfBound : public std::exception
{
public:
    virtual const char* what() const throw();
};

#endif /* !COLOR_OUT_OF_BOUND_H */
