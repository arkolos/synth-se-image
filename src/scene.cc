#include "scene.hh"
#include "objects/plane.hh"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>
#include "exceptions/parseerror-exception.hh"
#include "directional-light.hh"
#include "ambient-light.hh"
#include "point-light.hh"

#include "tbb/blocked_range2d.h"
#include "tbb/tbb.h"

namespace n_pt = boost::property_tree;

Light* parse_light(const n_pt::ptree::value_type &object)
{
  std::string type = object.second.get("type", "");
  std::string dir = object.second.get("dir", "");
  std::string color = object.second.get("color", "");
  float diff = 1;

  if (type == "directional")
     return new DirectionalLight(Color(color), diff, Vector3d(dir));
  else if (type == "point")
    {
      std::string pos = object.second.get("pos", "");
      return new PointLight(Color(color), diff, Point(pos));
    }
  throw ParseerrorException("Light type unknown : " + type);
}

PhysicalObject* parse_object(const n_pt::ptree::value_type &object)
{

    std::string type = object.second.get("type", "");
    const float diff = object.second.get("diff", 1.);
    const float refl = object.second.get("refl", 1.);
    const float opac = object.second.get("opac", 1.);
    const float mirr = object.second.get("mirr", 0.);

    if (type == "sphere")
    {
        float radius = object.second.get("radius", -1);
        std::string pos = object.second.get("pos", "");
        std::string color = object.second.get("color", "");
        if (radius <= 0)
            throw ParseerrorException("No radius found for sphere object");
        return new Sphere(Point(pos), radius, color, diff, refl, opac, mirr);
    }
    else if (type == "plane")
    {
        float a = object.second.get("a", 0);
        float b = object.second.get("b", 0);
        float c = object.second.get("c", 0);
        float d = object.second.get("d", 0);
        std::string color = object.second.get("color", "");
        return new Plane(a, b, c, d, color, diff, refl, opac, mirr);
    }

    throw ParseerrorException("Type object not found ("+ type + ")");
}

Scene::Scene(std::string filename)
{
  n_pt::ptree scene_tree;

  n_pt::read_xml(filename, scene_tree);

  width_ = scene_tree.get("scene.width", 640);
  height_ = scene_tree.get("scene.height", 480);

  std::string camera_position = scene_tree.get("scene.camera.pos", "");
  std::string camera_u = scene_tree.get("scene.camera.u", "");
  std::string camera_v = scene_tree.get("scene.camera.v", "");

  BOOST_FOREACH(const n_pt::ptree::value_type &object,
                scene_tree.get_child("scene.objects"))
    add_object(parse_object(object));

  BOOST_FOREACH(const n_pt::ptree::value_type &object,
                scene_tree.get_child("scene.lights"))
    add_light(parse_light(object));

  float coef = scene_tree.get("scene.alight.coef", 0.);
  std::string color = scene_tree.get("scene.alight.color", "");
  add_light(new AmbientLight(coef, Color(color)));

  camera_ = Camera(camera_position, camera_u, camera_v);
  max_ray_depth_ = 10;
}

void Scene::add_object(PhysicalObject* object)
{
  objects_.push_back(object);
}

void Scene::add_light(Light* light)
{
  lights_.push_back(light);
}

Scene::~Scene()
{
  for (Light* el : lights_)
    delete el;
  for (PhysicalObject* el : objects_)
    delete el;
}

Color Scene::get_color_inpoint(Point &point,
                               PhysicalObject* el,
                               const Ray& ray_from_observer,
                               unsigned int ray_depth) const
{
  Color c_diffuse;
  Color c_spec;
  Color c_transp;
  Color c_mirr;

  const Vector3d norm = el->normal_dir(point).normed();
  const Vector3d reflection = ray_from_observer.get_direction().get_reflection_with_normal(norm);

  int i = 0;
  for (Light* l : lights_)
  {//todo sum
    const Ray& ray_from_light = l->get_ray_topoint(point);
    Color c_light = get_light_color(l->get_color(), ray_from_light, NULL, el, ray_depth);

    //if (c_light.r() != l->get_color().r() || c_light.g() != l->get_color().g() || c_light.b() != l->get_color().b())
    //  std::cout << c_light << std::endl;

    if (c_light.r() != 0. || c_light.g() != 0. || c_light.b() != 0.)
    //if (!l->is_shadow_in_point(point, el, objects_))
    {
      const float coef = l->get_light_inpoint(point, norm);
      const Color c_light_diffuse = c_light * coef * el->diff();

      const float coef_spec = l->get_specular_inpoint(point, norm, reflection);
      const Color c_light_specular = c_light * coef_spec * el->refl();//to change for parameter from file

      c_diffuse = c_diffuse + c_light_diffuse;
      c_spec = c_spec + c_light_specular;
    }
    i++;
  }

  if (ray_depth < max_ray_depth_)
  {
    if (el->opac() < 1.)
    {
      const Ray transp_ray(point, ray_from_observer.get_direction());
      c_transp = get_nearest_element_color(transp_ray, el, ray_depth + 1, false);
    }

    if (el->mirr() > 0.)
    {
      const Ray mirr_ray(point, reflection);
      c_mirr = get_nearest_element_color(mirr_ray, el, ray_depth + 1, false) * el->mirr();
    }
  }

  const Color c = el->color() * (c_diffuse * el->opac() + c_transp * (1. - el->opac())) + c_spec + c_mirr;
  return c;
}

Color Scene::get_nearest_element_color(const Ray& ray, PhysicalObject* el_ignore, unsigned int ray_depth, bool from_screen) const
{
  Color nearest_obj_col;

  float min_distance = 1e308;
  PhysicalObject* nearest_obj = NULL;

  Point nearest_point(0,0,0); //this value will always have been changed if nearest_obj is set

  for (PhysicalObject* el : objects_)
  {//on cherche si le rayon passant par le point de l'écran intersepte des objects et si oui on cherche le plus proche
    if (el == el_ignore)
      continue;    

    const Point& visible = el->intersec_ray(ray);

    if ((visible.x() != 0 || visible.y() != 0 || visible.z() != 0) && ((from_screen && ray.is_after_unit_vect(visible)) || !from_screen)) // point existe et est derrière l'ecran
    {
      const float dist = Point::distance_square(ray.get_start(), visible);

      if (dist < min_distance)
      {
        min_distance = dist;
        nearest_obj = el;
        nearest_point = visible;
      }
    }
  }

  if (nearest_obj)
  {// si le rayon intercepte effectivement un objet, on cherche de quel couleur doit etre le point
    nearest_obj_col = get_color_inpoint(nearest_point, nearest_obj, ray, ray_depth);
  }

  return nearest_obj_col;
}

Color Scene::get_light_color(const Color& ray_col, const Ray& ray, PhysicalObject* el_ignore, PhysicalObject* el_final, unsigned int ray_depth) const
{
  Color light_col;

  float min_distance = 1e308;
  PhysicalObject* nearest_obj = NULL;

  Point nearest_point(0,0,0); //this value will always have been changed if nearest_obj is set

  for (PhysicalObject* el : objects_) 
  {
    if (el == el_ignore)
      continue;

    const Point& impact_pt = el->intersec_ray(ray);

    if (impact_pt.x() != 0 || impact_pt.y() != 0 || impact_pt.z() != 0)
    {
      const float dist = Point::distance_square(ray.get_start(), impact_pt);

      if (dist < min_distance)
      {
        min_distance = dist;
        nearest_obj = el;
        nearest_point = impact_pt;
      }
    }
  }

  if (nearest_obj == NULL || nearest_obj == el_final)
    return ray_col;
  else
  { 
    float opac = nearest_obj->opac();
    if (opac < 1.)
      light_col = (ray_col * (1. - opac)) * nearest_obj->color();

    if (light_col.r() == 0. && light_col.g() == 0. && light_col.b() == 0.)
      return light_col;
  }
  
  if (ray_depth < max_ray_depth_)
  {
    const Ray new_ray(nearest_point, ray.get_direction());
    return get_light_color(light_col, new_ray, nearest_obj, el_final, ray_depth + 1);
  }

  return light_col;
}

Ray Scene::get_rayscreen(int x, int y) const // return le rayon qui passe par le point de l'ecran
{
  const Vector3d u = camera_.get_u(), v = camera_.get_v();

  int DISTANCE_POINT_CAMERA = 10000;

  const Point point_screen = u * (x - width_ / 2)  + v * (height_ / 2 - y)  + camera_.get_origin();
  static const Vector3d behind_vect = camera_.calcul_w() * (-DISTANCE_POINT_CAMERA);

  const Point eye_point = behind_vect + camera_.get_origin();
  //Point point_screen = Point(x - width_ / 2, height_ / 2 - y , 0);
  return Ray(eye_point, point_screen);
}

void Scene::render(char *output_ppm) const
{
    Screenproj screen(width_, height_);

    tbb::parallel_for(
        tbb::blocked_range2d<int>(0, width_,  0, height_),
        [&](const tbb::blocked_range2d<int>& r)
    {
        for( int x=r.rows().begin(); x!=r.rows().end(); ++x )
            for( int y=r.cols().begin(); y!=r.cols().end(); ++y )
            {
                const Ray ray = get_rayscreen(x, y);
                const Color c = get_nearest_element_color(ray, NULL, 0, true);
                screen.set_color(x, y, c);
            }
    });

    screen.save(output_ppm);
}

