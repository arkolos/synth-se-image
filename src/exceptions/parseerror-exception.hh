#ifndef PARSERROREXCEPTION_HH
# define PARSERROREXCEPTION_HH

# include <exception>

# include <string>

class ParseerrorException : public std::exception
{
public:
  ParseerrorException(std::string err)
    : exception(),
      what_(err)
  {}
  virtual const char* what() const throw() override
  {
    return what_.c_str();
  }
private:
  std::string what_;
};
#endif /* !PARSERROREXCEPTION_HH */
