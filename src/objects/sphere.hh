#ifndef SPHERE_HH
# define SPHERE_HH

# include <math.h>
# include "../point.hh"
# include "../physical-object.hh"

class Sphere : public PhysicalObject
{
public:
  Sphere(Point center, float radius, Color c, float diff, float refl, float opac, float mirr)
    : PhysicalObject(c, diff, refl, opac, mirr)
    , center_(center)
    , radius_(radius)
  {}
  virtual Point intersec_ray(const Ray &ray) const override;
  virtual Vector3d normal_dir(const Point &point) const override;
  ~Sphere()
  {}

private:
  Point center_;
  float radius_;
};
#endif /* !SPHERE_HH */
