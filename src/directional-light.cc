#include "directional-light.hh"
#include <cassert>

float DirectionalLight::get_light_inpoint(Point,
                                                  Vector3d normal) const
{
  float product = normal.normed() * direction_.normed();
  return product < 0 ? -product : 0;
}


float DirectionalLight::get_specular_inpoint(const Point&, const Vector3d&, const Vector3d reflection_user) const
{

	const float product = - direction_.normed() * reflection_user.normed() ;

	return product > 0 ? product*product : 0;

}

static bool is_ray_intersecting_objects(Ray& ray, const std::vector<PhysicalObject*> objects, const PhysicalObject* object_not_considered)
{
    for (PhysicalObject* el : objects)
    {
        if (el != object_not_considered)
        {
            const Point p = el->intersec_ray(ray);
            if (!(p.x() == 0 && p.y() == 0 && p.z() == 0))
                return true;
        }
    }
    return false;
}

bool DirectionalLight::is_shadow_in_point(const Point& point, const PhysicalObject* el,const std::vector<PhysicalObject*>& objects) const
{
    Ray ray(point, -direction_);
    return is_ray_intersecting_objects(ray, objects, el);
}

Ray DirectionalLight::get_ray_topoint(const Point& point) const
{
    float max_dist = 20000;
    Ray ray((-direction_ * max_dist) + point, direction_);
    return ray;
}

