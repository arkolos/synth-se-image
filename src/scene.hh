#ifndef SCENE_HH
# define SCENE_HH

# include <list>

# include "physical-object.hh"
# include "objects/sphere.hh"
# include "light.hh"
# include "camera.hh"
# include "screenproj.hh"
# include <string>

class Scene
{
public:
  Scene(std::string filename);
  ~Scene();//free physical objects
  void add_light(Light& light);
  void add_object(PhysicalObject& object);
  void render(char *output_ppm) const;

private:
  void add_light(Light* light);
  void add_alight(Light* light);
  void add_object(PhysicalObject* object);
  Ray get_rayscreen(int x, int y) const;
  Color get_nearest_element_color(const Ray& ray, PhysicalObject* el_ignore, unsigned int ray_depth, bool from_screen) const;
  Color get_color_inpoint(Point &point, PhysicalObject* el, const Ray& ray_from_observer, unsigned int ray_depth) const;
  Color get_light_color(const Color& ray_col, const Ray& ray, PhysicalObject* el_ignore, PhysicalObject* el_final, unsigned int ray_depth) const;  

  int width_;
  int height_;
  std::vector<Light*> lights_;
  std::vector<Light*> alights_;
  std::vector<PhysicalObject*> objects_;
  unsigned int max_ray_depth_;
  Camera camera_;
};
#endif /* !SCENE_HH */
