#include "camera.hh"

Camera::Camera()
{
}

Camera::Camera(Point p, Vector3d u, Vector3d v)
  : origin_(p)
  , u_(u)
  , v_(v)
{}

Point Camera::get_origin() const
{
  return origin_;
}


Vector3d Camera::calcul_w() const
{
	const Vector3d ortho_vect = u_ ^ v_;

  	return ortho_vect.normed();
}
