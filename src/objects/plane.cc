#include "plane.hh"
#include <iostream>
#include <cassert>

Point Plane::get_included_point() const
{
    if (c_ != 0)
        return Point(0, 0, d_ / c_);
    if (b_ != 0)
        return Point(0, d_ / b_, 0);
    if (a_ != 0)
        return Point(d_ / a_, 0, 0);

    assert(false && "plan equation : a,b and c are equals to 0");

}

Point Plane::intersec_ray(const Ray &ray) const
{
    Point p = Point(0, 0, 0);


    //from https://en.wikipedia.org/wiki/Line%E2%80%93plane_intersection

    const Vector3d normal = normal_dir(p);
    const Vector3d direction = ray.get_direction().normed();

    const Vector3d P0_minus_LO(ray.get_start(),get_included_point()); 

    const float num = P0_minus_LO * normal;
    const float denom = direction * normal;
    
    if (num == 0 && denom == 0)
        return p;



    const float distance = num / denom;
    const Point intersec_p = direction * distance + ray.get_start();

    if (!ray.is_front_ray(intersec_p))
        return p;

    return intersec_p;
}

Vector3d Plane::normal_dir(const Point&) const
{
    return Vector3d(a_, b_, c_);
}
