#ifndef PLANE_HH
# define PLANE_HH

# include "../physical-object.hh"

class Plane : public PhysicalObject
{
public:
  Plane(float a, float b, float c, float d, Color col, float diff, float refl, float opac, float mirr)
    : PhysicalObject(col, diff, refl, opac, mirr)
    , a_(a)
    , b_(b)
    , c_(c)
    , d_(d)
  {}
  virtual Point intersec_ray(const Ray &ray) const override;
  virtual Vector3d normal_dir(const Point &point) const override;

private:
    float a_;
    float b_;
    float c_;
    float d_;
    Point get_included_point() const;
};

#endif /* !PLANE_H */
