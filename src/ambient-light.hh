#ifndef AMBIENT_LIGHT_HH
# define AMBIENT_LIGHT_HH

#include "light.hh"

class AmbientLight : public Light
{
public:
    AmbientLight(float coef, Color color)
        : Light(color, coef)
    {}
    virtual ~AmbientLight()
    {}
    virtual float get_light_inpoint(Point p, Vector3d normal) const override;
    virtual float get_specular_inpoint(const Point& point, const Vector3d& normal, const Vector3d reflection_user) const override;
    virtual bool is_shadow_in_point(const Point& point, const PhysicalObject* object,const std::vector<PhysicalObject*>& objects) const override;
    virtual Ray get_ray_topoint(const Point& point) const override;

private:
};

#endif /* !AMBIENT_LIGHT_H */
