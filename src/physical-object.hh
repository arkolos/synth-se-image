#ifndef PHYSICAL_OBJECT_HH
# define PHYSICAL_OBJECT_HH

# include "point.hh"
# include "vector3d.hh"
# include "ray.hh"
# include "color.hh"

class PhysicalObject
{
public:
  PhysicalObject(Color c, float diff, float refl, float opac, float mirr)
      : color_(c)
      , diff_(diff)
      , refl_(refl)
      , opac_(opac)
      , mirr_(mirr)
  {}
  virtual ~PhysicalObject()
  {}
  virtual Point intersec_ray(const Ray &ray) const = 0;
  virtual Vector3d normal_dir(const Point &point) const = 0;
  Color color() { return color_; }
  float diff() { return diff_; }
  float refl() { return refl_; }
  float opac() { return opac_; }
  float mirr() { return mirr_; }

private:
  Color color_;
  float diff_;
  float refl_;
  float opac_;
  float mirr_;
};
#endif /* !PHYSICAL_OBJECT_HH */

