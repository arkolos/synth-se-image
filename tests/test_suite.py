#! /usr/local/bin/python2.7
# -*- coding: utf-8 -*-

import os
from optparse import OptionParser

import coding_style.main

def main():
    coding_style.main.check("src/")

if __name__ == "__main__":
    main()
