#include "ambient-light.hh"

float AmbientLight::get_light_inpoint(Point p, Vector3d normal) const
{
    p.x();
    normal.normed();
    return diff_;
}


float AmbientLight::get_specular_inpoint(const Point& point, const Vector3d& normal, const Vector3d reflection_user) const
{
	return 0.0f;
}

bool AmbientLight::is_shadow_in_point(const Point& point, const PhysicalObject* object,const std::vector<PhysicalObject*>& objects) const
{
    return false;
}

Ray AmbientLight::get_ray_topoint(const Point& point) const
{
    return Ray(point, point);
}

