#include "vector3d.hh"
#include <cmath>

float Vector3d::length() const
{
  return sqrt(length_squared());
}

Vector3d Vector3d::operator*(int a) const
{
    return Vector3d(point_.x() * a, point_.y() * a, point_.z() * a);
}

Vector3d Vector3d::operator*(float a)const
{
    return Vector3d(point_.x() * a, point_.y() * a, point_.z() * a);
}

Point Vector3d::operator+(Point p) const
{
    return Point(point_.x() + p.x(), point_.y() + p.y(), point_.z() + p.z());
}

Vector3d Vector3d::operator+(Vector3d p) const
{
    return Vector3d(point_.x() + p.x(), point_.y() + p.y(), point_.z() + p.z());
}

float Vector3d::operator*(Vector3d other) const
{
  return point_.x() * other.point_.x() + point_.y() * other.point_.y()
    + point_.z() * other.point_.z();
}

Vector3d Vector3d::operator^(Vector3d other) const
{
  return Vector3d(point_.y() * other.point_.z() - point_.z() * other.point_.y(), 
                  point_.z() * other.point_.x() - point_.x() * other.point_.z(),
                  point_.x() * other.point_.y() - point_.y() * other.point_.x());
}

Vector3d Vector3d::normed() const
{
  float len = length();
  return Vector3d(point_.x() / len, point_.y() / len, point_.z() / len);
}

std::ostream& operator<<(std::ostream& ostr, Vector3d const& p)
{
  return ostr << p.get_point();
}


Vector3d Vector3d::get_reflection_with_normal(const Vector3d& normal) const //normal should be normalized
{
  return - (normal * (2 * (*this * normal)) - *this);
}
