#ifndef COLOR_HH
# define COLOR_HH


# include <vector>
# include <string>
# include <iostream>

class Color
{
public:
    Color();
    Color(float r, float g, float b);
    Color(std::string str); // str : X;Y;Z
    float r() const;
    float g() const;
    float b() const;
    std::ostream& print(std::ostream& ostr) const;
    Color operator+(const Color& c) const;
    Color operator*(float num) const;
    Color operator*(const Color& c) const;
    Color operator-(const Color& c)const ;
    Color operator-() const;
private:
    std::vector<float> color_;
};

std::ostream& operator<<(std::ostream& ostr, Color const& c);

#endif /* !COLOR_H */
