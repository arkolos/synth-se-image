#include "color.hh"
#include "exceptions/color-out-of-bound.hh"
#include "exceptions/parseerror-exception.hh"
#include <cstdlib>
#include <cassert>

Color::Color()
{
  color_.resize(3);
}

Color::Color(float r, float g, float b)
{
    if (r < 0 || r > 1 || g < 0 || g > 1 || b < 0 || b > 1)
        throw ColorOutOfBound();
    else
    {
        color_.resize(3);
        color_[0] = r;
        color_[1] = g;
        color_[2] = b;
    }
}

Color::Color(std::string str)
{
  size_t pos1 = str.find_first_of(";", 0);
  if (pos1 == std::string::npos)
    throw ParseerrorException("Parsing color 1 " + str);
  size_t pos2 = str.find_first_of(";", pos1 + 1);
  if (pos2 == std::string::npos)
    throw ParseerrorException("Parsing color 2 " + str);
  float r = std::atof(str.substr(0, pos1).c_str());
  float g = std::atof(str.substr(pos1 + 1, pos2 - pos1 - 1).c_str());
  float b = std::atof(str.substr(pos2 + 1).c_str());

  color_.resize(3);
  color_[0] = r;
  color_[1] = g;
  color_[2] = b;
}

float Color::r() const
{
    return color_[0];
}

float Color::g() const
{
    return color_[1];
}

float Color::b() const
{
    return color_[2];
}

Color Color::operator+(const Color& c) const
{
    float r = color_[0] + c.r();
    float g = color_[1] + c.g();
    float b = color_[2] + c.b();
    if (r > 1)
        r = 1;
    if (g > 1)
        g = 1;
    if (b > 1)
        b = 1;
    return Color(r, g, b);
}

Color Color::operator-(const Color& c) const
{
    float r = color_[0] - c.r();
    float g = color_[1] - c.g();
    float b = color_[2] - c.b();
    if (r < 0)
        r = 0;
    if (g < 0)
        g = 0;
    if (b < 0)
        b = 0;
    return Color(r, g, b);
}

Color Color::operator*(float num) const
{
    assert(num >= 0 && num <= 1);
    float r = color_[0];
    float g = color_[1];
    float b = color_[2];

    return Color(r * num, g * num, b * num);
}

Color Color::operator*(const Color& c) const
{
    return Color(r() * c.r(), g() * c.g(), b() * c.b());
}

Color Color::operator-() const
{
    float r = 1 - color_[0];
    float g = 1 - color_[1];
    float b = 1 - color_[2];
    return Color(r, g, b);
}

std::ostream& operator<<(std::ostream& ostr, Color const& c)
{
    int r = c.r() * 255;
    int g = c.g() * 255;
    int b = c.b() * 255;
    ostr << r << " " << g << " " << b;
    return ostr;
}
