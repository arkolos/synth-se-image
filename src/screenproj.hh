#ifndef SCREENPROJ_HH
# define SCREENPROJ_HH

# include <string>
# include <fstream>
# include "color.hh"

class Screenproj
{
public:
    Screenproj(int width, int height);
    void set_color(int x, int y, const Color& c);
    void save(const char *output_ppm);
    int get_width()
    {
        return width_;
    }
    int get_height()
    {
        return height_;
    }
private:
    Screenproj& operator=(const Screenproj s);
    std::vector< std::vector<Color> > image_;
    const int width_;
    const int height_;
};

#endif /* !SCREENPROJ_HH */
