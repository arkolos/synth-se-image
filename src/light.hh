#ifndef LIGHT_HH
# define LIGHT_HH

# include "color.hh"
# include "point.hh"
# include "ray.hh"
# include "vector3d.hh"
# include <list>

# include "physical-object.hh"

class Light
{
public:
  Light(Color& color, float diff)
    : color_(color)
    , diff_(diff)
  {}
  virtual ~Light()
  {}
  Color get_color() const
  {
    return color_;
  }
  float get_diff() const
  {
    return diff_;
  }

  virtual float get_light_inpoint(Point point, Vector3d normal) const = 0;
  virtual float get_specular_inpoint(const Point& point, const Vector3d& normal, const Vector3d reflection_user) const = 0;
  virtual bool is_shadow_in_point(const Point& point, const PhysicalObject* object, const std::vector<PhysicalObject*>& objects) const = 0;
  virtual Ray get_ray_topoint(const Point& point) const = 0;

private:
  Color get_color_inpoint(Point);
  const Color color_;
protected:
  const float diff_;
};
#endif /* !LIGHT_HH */
