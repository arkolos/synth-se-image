#include "point.hh"

#include <iostream>
#include "exceptions/parseerror-exception.hh"
#include <string>
#include <cstdlib>
#include <math.h>

Point::Point(std::string str)
{
  x_ = 0;
  y_ = 0;
  z_ = 0;

  size_t pos1 = str.find_first_of(";", 0);
  if (pos1 == std::string::npos)
    throw ParseerrorException("Parsing point 1 " + str);
  size_t pos2 = str.find_first_of(";", pos1 + 1);
  if (pos2 == std::string::npos)
    throw ParseerrorException("Parsing point 2 " + str);


  x_ = std::atof(str.substr(0, pos1).c_str());
  y_ = std::atof(str.substr(pos1 + 1, pos2 - pos1 - 1).c_str());
  z_ = std::atof(str.substr(pos2 + 1).c_str());

}

std::ostream& operator<<(std::ostream& ostr, Point const& p)
{
  ostr << "(" << p.x() << ";" << p.y() << ";" << p.z() << ")";
  return ostr;
}

Point Point::operator+(const Point p)
{
  return Point(x_ + p.x(), y_ + p.y(), z_ + p.z());
}

Point Point::operator-(const Point p)
{
  return Point(x_ - p.x(), y_ - p.y(), z_ - p.z());
}
