#! /usr/local/bin/env python2.7
# -*- coding: utf-8 -*-

import os
import subprocess

import color_print

def is_source(f):
    return os.path.splitext(f)[1] == ".cc" or os.path.splitext(f)[1] == ".hh" or os.path.splitext(f)[1] == ".hxx"

def print_error(s, line, f):
    print(s + " " + str(line) + " in " + f)

# checks correct style of a .c
def check_source(source_file):
    ok = 0
    line = 1
    char_nb = 0
    prev = 'a'
    curr_func_line = 0
    func_len = 0
    f = open(source_file, 'r')
    while 1:
        char = f.read(1)
        char_nb += 1
        if not char: break
        if char == '{':
            func_len = 0
            curr_func_line = line - 1
        if char == '}':
            func_len = 0
        if char == '\n':
            if prev == ' ':
                print_error("space found at end of line", str(line), source_file)
                ok = 1
            line = line + 1
            char_nb = 0
            if prev <> '\n':
                func_len += 1
            if func_len == 26 :
                print_error("function of more than 25 lines at line", str(curr_func_line), source_file)
                ok = 1
        if char == '\t':
            print_error("tab found at line", str(line), source_file)
            ok = 1
        prev = char
        if char_nb == 81:
            print_error("more than 80 characters at line", str(line), source_file)
            ok = 1
    f.close()
    return ok

def rec_check(directory):
    errors = 0
    for name in os.listdir(directory):
        f = os.path.join(directory, name)
        if os.path.isdir(f):
            errors += rec_check(f)
        else:
            if is_source(f):
                errors += check_source(f)
    return errors

# main function: checks coding style of sources and headers
# in given directory and sub-directories recursively
def check(directory):
    errors = 0
    for name in os.listdir(directory):
        f = os.path.join(directory, name)
        if os.path.isdir(f):
            errors += rec_check(f)
        else:
            if is_source(f):
                errors += check_source(f)
    print("Coding style:\t"),
    if errors == 0:
        color_print.green("ok");
    else:
        color_print.fail("%d bad file(s)" % errors)
