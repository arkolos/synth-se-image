#include "ray.hh"

#include <cassert>
#include <iostream>


# include "physical-object.hh"

float Ray::coef_position_point(const Point& p) const
{
    const Vector3d vect_points = Vector3d(start_, p);

    if (direction_.x() != 0)
        return (vect_points.x()) / direction_.x();

    if (direction_.y() != 0)
        return (vect_points.y()) / direction_.y();

    if (direction_.z() != 0)
        return (vect_points.z()) / direction_.z();

    //assert(false);
    return 0.;
}

std::ostream& operator<<(std::ostream& ostr, Ray const& c)
{
 	ostr << "point : " <<  c.get_start() << " ; direction : " << c.get_direction() << "( point ecran ) " << c.get_direction() + c.get_start();
 	return ostr;


}
