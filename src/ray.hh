#ifndef RAY_HH
# define RAY_HH

# include "vector3d.hh"
# include <list>

class PhysicalObject;

class Ray
{
public:
  Ray(const Point& start, const Vector3d& direction)
    : start_(start)
    , direction_(direction)
  {}
  Ray(const Point& point1, const Point& point2)
      : start_(point1)
      , direction_(Vector3d(point1, point2))
    {}
  Vector3d get_direction()const
  {
      return direction_;
  }
  const Point& get_start()const
  {
      return start_;
  }

  float coef_position_point(const Point& p) const;// is < 0 if point behind ray. > 0 if is in the good direction . < 1 if before the unit vect 

  bool is_after_unit_vect(const Point& p) const//p should be on the ray. return true if p > start+direction. Used to prevent to display something before the screen 
  {
    return coef_position_point(p) > 1.; 
  }
  bool is_front_ray(const Point& p) const//p should be on the ray. return true if p > start suivant la direction du rayon
  {
    return coef_position_point(p) > 0.;
  }


private:
  const Point start_;
  const Vector3d direction_;
};

std::ostream& operator<<(std::ostream& ostr, Ray const& c);

#endif /* !RAY_HH */
