#ifndef POINTLIGHT_HH
# define POINTLIGHT_HH

# include "vector3d.hh"
# include "point.hh"
# include "light.hh"

class PointLight : public Light
{
public:
  PointLight(Color color, float diff, Point origin)
    : Light(color, diff)
    , origin_(origin)
  {}
  ~PointLight()
  {}
  virtual float get_light_inpoint(Point point, Vector3d normal) const override;
  virtual float get_specular_inpoint(const Point& point, const Vector3d& normal, const Vector3d reflection_user) const override;
  virtual bool is_shadow_in_point(const Point& point, const PhysicalObject* object,const std::vector<PhysicalObject*>& objects) const override;
  virtual Ray get_ray_topoint(const Point& point) const override;

private:
  const Point origin_;
};
#endif /* !POINTLIGHT_HH */
